use std::env;
use std::process::exit;
use std::str::FromStr;

struct PathPattern<'a> {
    lines: &'a Vec<String>,
    width: usize,
}

impl PathPattern<'_> {
    fn new(lines: &Vec<String>) -> PathPattern {
        let width = lines.get(0).map_or(0, |line| line.len());
        PathPattern { lines, width }
    }

    fn next_pos(&self, x: usize, velocity: &usize) -> usize {
        (x + velocity) % self.width
    }

    fn count_tree_path(&self, y: &usize, x: &usize) -> usize {
        let mut pos = 0;
        let mut idx = 0;
        self.lines
            .iter()
            .fold(0, |count, line| {
                if (idx % y) == 0{
                    let current_is_tree = line.get(pos..(pos + 1)).map_or(false, |case| case == "#");
                    pos = self.next_pos(pos, x);
                    idx = idx + 1;
                    if current_is_tree {
                        count + 1
                    } else {
                        count
                    }
                } else {
                    idx = idx + 1;
                    count
                }
            })
    }
}

fn main() {
    println!("run day 3 aoc 2020");
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let slopes = [(1,1), (1,3), (1,5), (1,7), (2,1)];
    read_all::<String>(filename.as_str())
        .iter()
        .fold(Ok(Vec::new()), |res: Result<Vec<String>, &str>, item| {
            match res {
                Ok(mut vec) => {
                    match item {
                        Ok(line) => {
                            vec.push(line.to_string());
                            Ok(vec)
                        }
                        Err(error) => {
                            println!("parseError: {:?}", error);
                            exit(1);
                        }
                    }
                }
                Err(e) => Err(e)
            }
        })
        .map(|lines| {
            let count = slopes.iter()
                .fold(1, |acc, (y,x)|{
                    let count = PathPattern::new(&lines).count_tree_path(y, x);
                    println!("slope {:?},{:?}: {:?}", y, x, count);
                    acc * count
                });
            println!("count: {:?}", count);
            exit(0);
        });

    println!("unknow error");
    exit(2);
}

fn read_all<T: FromStr>(file_name: &str) -> Vec<Result<T, <T as FromStr>::Err>> {
    std::fs::read_to_string(file_name)
        .expect("file not found!")
        .lines()
        .map(|x| x.parse())
        .collect()
}