use std::env;
use std::process::exit;
use std::str::FromStr;

fn main() {
    println!("run day 1 aoc 2020");
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let read =  read_all::<u64>(filename.as_str());
    let mut numbers: Vec<u64> = read
        .iter()
        .fold( Vec::new(), |mut res: Vec<u64>, item| {
            match item {
                Ok(int) => { res.push(int.clone()) }
                Err(error) => {
                    println!("parseError: {:?}", error);
                    exit(1);
                }
            };
            res
        });
    numbers.sort();
    let max = numbers.len();
    let mut i= 0;
    let mut j = 1;
    while i < max {
        while j < max {
            if numbers[i] + numbers[j] == 2020 {
                println!("{:?}", numbers[i] * numbers[j]);
                exit(0);
            } else {
                j = j + 1;
            }
        }
        i = i + 1;
        j = i +1;
    }
    println!("no result found");
    exit(2);
}

fn read_all<T: FromStr>(file_name: &str) -> Vec<Result<T, <T as FromStr>::Err>> {
    std::fs::read_to_string(file_name)
        .expect("file not found!")
        .lines()
        .map(|x| x.parse())
        .collect()
}