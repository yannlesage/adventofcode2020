use std::env;
use std::process::exit;
use std::str::FromStr;
use regex::Regex;

struct PasswordData {
    letter: String,
    min: usize,
    max: usize,
    password: String,
}

impl PasswordData {
    fn is_valid_v1(pd: &PasswordData) -> bool {
        let count = pd.password.matches(&pd.letter).count();
        return count <= pd.max && count >= pd.min;
    }

    fn is_valid_v2(pd: &PasswordData) -> bool {
        let pass = pd.password.as_bytes();
        let first_occ = pass[pd.min-1] as char;
        let second_occ = pass[pd.max-1] as char;
        let letter = pd.letter.as_bytes()[0] as char;
        !((first_occ == letter && second_occ == letter) ||
            (first_occ != letter && second_occ != letter))
    }
}

impl FromStr for PasswordData {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"^(\d+)-(\d+) (.): (.*)$").unwrap();
        re.captures(s).map_or(Err("Failed to find PasswordData".to_string()), |captures| {
            captures.get(3)
                .map(|x| x.as_str().to_string())
                .map_or(
                Err("Failed to read PassawordData".to_string()),
                |letter| {
                    captures.get(1)
                        .map_or(
                            Err("Failed to read min occurrence".to_string()),
                            |x| x.as_str().parse::<usize>()
                                .map_or(
                                    Err("Parse min to find failed".to_string()), |min| {
                                        captures.get(2)
                                            .map_or(
                                                Err("Failed to read max occurrence".to_string()),
                                                |x| x.as_str().parse::<usize>()
                                                    .map_or(
                                                        Err("Parse max to find failed".to_string()), |max| {
                                                            captures.get(4)
                                                                .map(|x| x.as_str().to_string())
                                                                .map_or(
                                                                    Err("Failed to read PassawordData".to_string()),
                                                                    |password|
                                                                        Ok( PasswordData { letter, min, max, password }),
                                                                    )
                                                        }),
                                            )
                                    }),
                        )
                })
        })
    }
}

fn main() {
    println!("run day 2 aoc 2020");
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    let version = if(args.len() > 2){
        &args[2][..]
    } else {
        "v1"
    };
    let password_policies = match version {
        "v2" => PasswordData::is_valid_v2,
        _ => PasswordData::is_valid_v1
    };
    let valid_pass_count = read_all::<PasswordData>(filename.as_str())
        .iter()
        .fold(0, |res, item| {
            match item {
                Ok(password) => {
                    if password_policies(password) {
                        return res + 1;
                    } else {
                        return res;
                    }
                }
                Err(error) => {
                    println!("parseError: {:?}", error);
                    exit(1);
                }
            }
        });

    println!("count: {:?}", valid_pass_count);
    exit(0);
}

fn read_all<T: FromStr>(file_name: &str) -> Vec<Result<T, <T as FromStr>::Err>> {
    std::fs::read_to_string(file_name)
        .expect("file not found!")
        .lines()
        .map(|x| x.parse())
        .collect()
}